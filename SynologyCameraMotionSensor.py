from enum import Enum    
from datetime import datetime, timedelta
import uuid
from synology.surveillance_station import SurveillanceStation
import appdaemon.plugins.hass.hassapi as hass


class LogLevels(Enum):
    SC_ERR = 3
    SC_WARNING = 4
    SC_NOTICE = 5
    SC_INFO = 6
    SC_DEBUG = 7 

    def __int__(self):
        return self.value



class SynologyCameraMotionSensor(hass.Hass):
    conf = None
    
    def initialize(self):
        self.conf = {}

        try: 
            self._verify_ssl = self.args["verify_ssl"]
        except KeyError:
            self._verify_ssl = True

        try: 
            self._connection_timeout = self.args["verify_ssl"]
        except KeyError:
            self._connection_timeout = 10

        try: 
            self.conf["log_control"] = self.args["log_control"]
        except KeyError: 
            self.conf["log_control"] = None

        self.set_log_level()
        if self.conf["log_control"] != None:
            self.listen_state(self.log_control_change_callback, self.conf["log_control"])

        try:
            self.conf["polling_interval"] = self.args["polling_interval"]
        except KeyError:
            self.conf["polling_interval"] = 10

        # This will call Synology API to get SID (session ID), fetch all the cameras/motion settings and cache them
        self.conf["surveillance"] = SurveillanceStation(self.args["api_url"], self.args["api_username"], self.args["api_password"], verify_ssl=self._verify_ssl, timeout=self._connection_timeout)

        try:
            self.load_camera_configs(self.args["cameras"])
        except KeyError:
            self.sc_log(LogLevels.SC_WARNING, "No cameras list found in configuration")

        # TODO: Refresh connection periodically (once per day?) 

        self.listen_on_sensitivity_inputs()
        self.listen_on_threshold_inputs()
        self.preload_camera_last_events()
        self.schedule_camera_polls()

    def sc_log(self, level, msg):
        try: 
            int(level)
        except (TypeError, ValueError) as e:
            self.log("WARNING: Invalid usage of sc_log - non-integer level parameter: %s.  Using SC_WARNING" % level)
            level = LogLevels.SC_WARNING

        try:
            log_level = self.conf["log_level"]
            if log_level == None:
                log_level = int(LogLevels.SC_INFO)
            else:
                log_level = int(self.conf["log_level"])
        except KeyError:
            log_level = int(LogLevels.SC_INFO)
        if int(level) > log_level:
            return

        if level == LogLevels.SC_ERR:
            lvl = "ERROR:"
        elif level == LogLevels.SC_WARNING:
            lvl = "WARNING:"
        elif level == LogLevels.SC_NOTICE:
            lvl = "NOTICE:"
        elif level == LogLevels.SC_INFO:
            lvl = "INFO:"
        elif level == LogLevels.SC_DEBUG:
            lvl = "DEBUG:"
        else: 
            self.log("WARNING: Invalid usage of sc_log - level parameter out of range: %d. Using.SC_WARNING" % level)
            level = LogLevels.SC_WARNING
            lvl = "WARNING:"

        self.log("%s %s" % (lvl, msg))


    def set_log_level(self):
        if self.conf["log_control"] == None:
            self.sc_log(LogLevels.SC_NOTICE, "Log level updated to None - defaulting to INFO")
            return

        value = self.get_state(self.conf["log_control"])
        if value == "ERROR":
            self.conf["log_level"] = LogLevels.SC_ERR
        elif value == "WARNING":
            self.conf["log_level"] = LogLevels.SC_WARNING
        elif value == "NOTICE":
            self.conf["log_level"] = LogLevels.SC_NOTICE
        elif value == "INFO":
            self.conf["log_level"] = LogLevels.SC_INFO
        elif value == "DEBUG":
            self.conf["log_level"] = LogLevels.SC_DEBUG
        else:
            self.sc_log(LogLevels.SC_NOTICE, "Log level update to invalid state: %s - defaulting to INFO" % value)
            return

        self.sc_log(LogLevels.SC_NOTICE, "Log level updated to %s" % value)


    def log_control_change_callback(self, entity, attribute, old, new, kwargs): 
        self.set_log_level()

        
    def set_config_val(self, input_key, conf_key=None, required=False, num=False, time=False):
        tmpval = None
        try:
            tmpval = self.args[input_key]
        except KeyError: 
            if not required:
                self.sc_log(LogLevels.SC_NOTICE, "%s not found in configuration" % input_key)
                return False
            else:
                raise ValueError("%s not found in configuration" % input_key)
        
        if num == True:
            try:
                int(tmpval)
            except (TypeError, ValueError) as e:
                raise ValueError("%s must be an integer.  Found %s" % (input_key, tmpval))

        if time == True:
            try:
                int(tmpval)
            except (TypeError, ValueError) as e:
                if tmpval != "sunrise" and tmpval != "sunset":
                    raise ValueError("%s must be a time value - either HHMM (24hr) or \"sunrise\" or \"sunset\". Found %s" % (input_key, tmpval))

        if conf_key == None:
            conf_key = input_key
        self.conf[conf_key] = tmpval
        return True


    def load_camera_configs(self, args):
        self.sc_log(LogLevels.SC_INFO, "Loading camera configuration")

        self.conf["cameras"] = {}
        self.conf["sensors"] = {}
        self.conf["sensitivity_inputs"] = {}
        self.conf["threshold_inputs"] = {}
        self.conf["last_events"] = {}
        synology_cameras = self.conf["surveillance"].get_all_cameras()

        for camera_args in args:
            try:
                camera_name = camera_args["camera"]
            except KeyError:
                self.sc_log(LogLevels.SC_WARNING, "Camera configuration must include a camera name")
                continue

            try:
                camera_sensor = camera_args["motion_sensor"]
            except KeyError:
                self.sc_log(LogLevels.SC_WARNING, "Camera configuration must include a motion_sensor")
                continue
                
            try:
                sensitivity_input = camera_args["sensitivity_input"]
            except KeyError:
                sensitivity_input = None

            try:
                threshold_input = camera_args["threshold_input"]
            except KeyError:
                threshold_input = None

            found_cam = None
            for syn_cam in synology_cameras:
                if syn_cam.name == camera_name:
                    found_cam = syn_cam
                    break

            if not found_cam: 
                self.sc_log(LogLevels.SC_WARNING, "Camera %s not found in Synology Surveillance Station" % camera_name)
                continue

            state = self.get_state(camera_sensor)
            if not state or state == "{}":
                self.sc_log(LogLevels.SC_WARNING, "Camera %s configured motion_sensor %s not found in homeassistant" % (camera_name, camera_sensor))
                continue

            self.conf["cameras"][found_cam.camera_id] = syn_cam
            self.conf["sensors"][found_cam.camera_id] = camera_sensor
            self.conf["last_events"][found_cam.camera_id] = None
            self.sc_log(LogLevels.SC_INFO, "Monitoring camera %s (%d) and reflecting motion in sensor entity %s" % (camera_name, found_cam.camera_id, camera_sensor))
            if sensitivity_input:
                self.conf["sensitivity_inputs"][sensitivity_input] = found_cam.camera_id
                sensitivity_value = self.conf["surveillance"].get_motion_setting(found_cam.camera_id).sensitivity
                self.set_state(sensitivity_input, state = sensitivity_value)
                self.sc_log(LogLevels.SC_INFO, "Monitoring input %s for sensitivity changes to apply to camera %s (%d) - current value %s" % 
                                (sensitivity_input, camera_name, found_cam.camera_id, sensitivity_value))
            if threshold_input:
                self.conf["threshold_inputs"][threshold_input] = found_cam.camera_id
                threshold_value = self.conf["surveillance"].get_motion_setting(found_cam.camera_id).threshold
                self.set_state(threshold_input, state = threshold_value)
                self.sc_log(LogLevels.SC_INFO, "Monitoring input %s for threshold changes to apply to camera %s (%d) - current value %s" % 
                                (threshold_input, camera_name, found_cam.camera_id, threshold_value))
           


    def preload_camera_last_events(self):
        for camera_id in self.conf["cameras"].keys():
            events = self.conf["surveillance"].get_event_list(limit=1, cameraIds=[ camera_id ])
            if events and events[0]:
                self.conf["last_events"][camera_id] = events[0]
                if not events[0].is_complete:
                    self.sc_log(LogLevels.SC_NOTICE, "Motion detected on camera %s (%d)" % (self.conf["cameras"][camera_id].name, camera_id))
                    self.turn_on(self.conf["sensors"][camera_id])


    def schedule_camera_polls(self):
        for camera_id in self.conf["cameras"].keys():
            self.run_at(self.camera_poll_events_callback, datetime.now() + timedelta(seconds=self.conf["polling_interval"]), camera_id=camera_id)


    def listen_on_sensitivity_inputs(self):
        for sensitivity_input, camera_id in self.conf["sensitivity_inputs"].items():
            self.listen_state(self.sensitivity_input_change_callback, sensitivity_input)


    def sensitivity_input_change_callback(self, entity, attribute, old, new, kwargs):
        self.sc_log(LogLevels.SC_DEBUG, "Sensitivity input %s changed from %s to %s" % (entity, old, new))

        try:
            camera_id = self.conf["sensitivity_inputs"][entity]
        except KeyError:
            self.sc_log(LogLevels.SC_ERR, "Camera ID for sensitivity input %s not found" % entity)
            return

        try:
            camera = self.conf["cameras"][camera_id]
        except KeyError:
            self.sc_log(LogLevels.SC_ERR, "Camera with ID %s not found for sensitivity input %s" % (camera_id, entity))
            return

        self.sc_log(LogLevels.SC_NOTICE, "Changing sensitivity level for camera %s (%d) to %s" % (camera.name, camera_id, new))
        self.conf["surveillance"].adjust_motion_detection_params(camera_id, sensitivity=new)


    def listen_on_threshold_inputs(self):
        for threshold_input, camera_id in self.conf["threshold_inputs"].items():
            self.listen_state(self.threshold_input_change_callback, threshold_input)


    def threshold_input_change_callback(self, entity, attribute, old, new, kwargs):
        self.sc_log(LogLevels.SC_DEBUG, "Threshold input %s changed from %s to %s" % (entity, old, new))

        try:
            camera_id = self.conf["threshold_inputs"][entity]
        except KeyError:
            self.sc_log(LogLevels.SC_ERR, "Camera ID for threshold input %s not found" % entity)
            return

        try:
            camera = self.conf["cameras"][camera_id]
        except KeyError:
            self.sc_log(LogLevels.SC_ERR, "Camera with ID %s not found for threshold input %s" % (camera_id, entity))
            return

        self.sc_log(LogLevels.SC_NOTICE, "Changing threshold level for camera %s (%d) to %s" % (camera.name, camera_id, new))
        self.conf["surveillance"].adjust_motion_detection_params(camera_id, threshold=new)


    def camera_poll_events_callback(self, kwargs):
        try: 
            camera_id = kwargs["camera_id"]
        except KeyError:
            self.sc_log(LogLevels.SC_ERR, "Scheduled poll for camera events was not provided a camera ID parameter")
            return

        events = self.conf["surveillance"].get_event_list(limit=1, cameraIds=[ camera_id ])
        for event in events:
            last_event = self.conf["last_events"][camera_id] 
            if not last_event or last_event.event_id != event.event_id:
                self.sc_log(LogLevels.SC_DEBUG, "New event found: %s event_id %s camera_id %s start_time %s stop_time %s reason %s" %
                        (event.camera_name, event.event_id, event.camera_id, event.start_time, event.stop_time, event.reason))
                self.conf["last_events"][camera_id] = event
                
                if last_event != None and event.start_time <= last_event.start_time:
                    self.sc_log(LogLevels.SC_DEBUG, "New event %s is older than previous known event, indicating previous event was deleted.  Not triggering motion alarm" %
                                    event.event_id)
                    if event.is_complete and self.get_state(self.conf["sensors"][camera_id]) == 'on':
                        self.sc_log(LogLevels.SC_NOTICE, "Motion event on camera %s (%d) ended" % (self.conf["cameras"][camera_id].name, camera_id))
                        self.turn_off(self.conf["sensors"][camera_id])
                elif self.get_state(self.conf["sensors"][camera_id]) != "on":
                    self.sc_log(LogLevels.SC_NOTICE, "Motion detected on camera %s (%d)" % (self.conf["cameras"][camera_id].name, camera_id))
                    self.turn_on(self.conf["sensors"][camera_id])
                    if event.is_complete:
                        self.sc_log(LogLevels.SC_NOTICE, "Motion event on camera %s (%d) ended" % (self.conf["cameras"][camera_id].name, camera_id))
                        self.turn_off(self.conf["sensors"][camera_id])
                
            elif not last_event.is_complete and event.is_complete:
                self.conf["last_events"][camera_id] = event
                self.sc_log(LogLevels.SC_NOTICE, "Motion event on camera %s (%d) ended" % (self.conf["cameras"][camera_id].name, camera_id))
                self.turn_off(self.conf["sensors"][camera_id])

        self.run_at(self.camera_poll_events_callback, datetime.now() + timedelta(seconds=self.conf["polling_interval"]), camera_id=camera_id)
